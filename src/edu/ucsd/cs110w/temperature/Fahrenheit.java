package edu.ucsd.cs110w.temperature; 
public class Fahrenheit extends Temperature 
{ 
 public Fahrenheit(float t) 
 { 
 super(t); 
 } 
 public String toString() 
 { 
 // TODO: Complete this method 
 return (String.valueOf(getValue())+ " F"); 
 }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	 return new Celsius((getValue()-32)/(float)1.8);    
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	 return new Fahrenheit(getValue());
}
public Temperature toKelvin() {
	// TODO Auto-generated method stub
	return new Kelvin((getValue()-32)/(float)1.8+(float)273.15);
}

}