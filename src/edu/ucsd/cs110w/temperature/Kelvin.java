package edu.ucsd.cs110w.temperature;

public class Kelvin extends Temperature{

	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO: Complete
		return (String.valueOf(getValue())+ " K");
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius(getValue()-(float)273.15);
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit((getValue()-(float)273.15)*(float)1.8+32);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Kelvin(getValue());
	}

}
